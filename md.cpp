#include "Space.h"
#include "Parser/Parser.h"
#include "Utils/Utils.h"
#include <chrono>

int main(const int argc, char* argv[]) {
    Parser pars(argc, argv);
    std::ifstream fin(pars.getCfgInpFile());
    std::string tStr;
    std::vector<double> E_tr;
    std::vector<double> E_rot;
    std::vector<double> E_vib;
    std::vector<double> alpha;
    size_t N = 50;
    while (std::getline(fin, tStr)) {
        std::vector<std::string> params = Utils::parseLine3(tStr);
        if (params.size() != 4) {
            std::cerr << "Bad input for molecule" << std::endl;
            return 1;
        }
        E_tr.push_back(stod(params[0]));
        E_rot.push_back(stod(params[1]));
        E_vib.push_back(stod(params[2]));
        alpha.push_back(stod(params[3]));
    }
    fin.close();

    Outer out("N.out");
    auto wholeStart = std::chrono::steady_clock::now();

    for (size_t i = 0; i < E_tr.size(); ++i) {
        std::cout << E_tr[i] << "  " << E_rot[i] << "  " << alpha[i] << std::endl;
        
        std::vector<double> vel(3, 0);
        double eRot = 0, eVib = 0;
        size_t steps = 0;
        auto confStart = std::chrono::steady_clock::now();
        Outer outT("out_" + std::to_string(i));
        for (size_t j = 0; j < N; ++j) {
            std::cout << "----- N = " << j + 1 << " -----" << std::endl;
            Space* space = new Space();
            srand(time(nullptr));
            space->setConfig(pars.getCfgFile());
            space->init(pars.getInpFile());
            space->initFromParams(E_tr[i], E_rot[i], E_vib[i], alpha[i]);

            size_t step = 0;
            auto start = std::chrono::steady_clock::now();
            space->vtkNum = 0;
            while (!space->MDStep() && step < 50000) {
                if (step % 100 == 0) {
                    auto tmp = std::chrono::steady_clock::now();
                    std::cout << "step: " << step << "  |  " << static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(tmp - start).count()) / 1000 << " sec" << std::endl;
                }
                // if (step % 100 == 0) space->writeVTK(pars.getOutFile() + "_" + std::to_string(i) + "_" + std::to_string(j));
                ++step;
            }
            // space->writeVTK(pars.getOutFile() + "_" + std::to_string(i) + "_" + std::to_string(j));
            auto tmp = std::chrono::steady_clock::now();
            double timeT = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(tmp - start).count()) / 1000;
            std::cout << "duration of calculation: " << timeT << " sec" << std::endl;
            double eV = 0, eR = 0;
            std::vector<double> vTmp(3, 0);
            if (!space->molsN2.empty()) {
                for (size_t in = 0; in < 3; ++in)
                    vTmp[in] = (space->molsN2[0].atom[0]->vel[in] + space->molsN2[0].atom[1]->vel[in]) / 2.0;

                eV = space->molsN2[0].atom[0]->eVib / KB;
                eR = space->molsN2[0].atom[0]->eRot / KB;
            }
            for (size_t in = 0; in < 3; ++in) vel[in] += vTmp[in];
            eVib += eV;
            eRot += eR;
            steps += step;
            saveInfo(outT, E_tr[i], E_rot[i], E_vib[i], alpha[i], vTmp, eV, eR, 1, timeT, step);
            std::cout << vTmp[0] << " " << vTmp[1] << " " << vTmp[2] 
                << "  |  E_rot = " << eR << " E_vib = " << eV << "  |  " << step << " " << timeT << std::endl;
            delete space;
        }
        
        auto tmp = std::chrono::steady_clock::now();
        double confTime = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(tmp - confStart).count()) / 1000;
        saveInfo(out, E_tr[i], E_rot[i], E_vib[i], alpha[i], vel, eVib, eRot, N, confTime, steps);
        std::cout << vel[0] / N << " " << vel[1] / N << " " << vel[2] / N 
                << "  |  E_rot = " << eRot / N << " E_vib = " << eVib / N 
                    << "  |  " << steps << " " << confTime << std::endl;

    }

    auto tmp = std::chrono::steady_clock::now();
    std::cout << "full duration of calculation: " << static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(tmp - wholeStart).count()) / 1000 << " sec" << std::endl;
    
    return 0;
}   

// int main(const int argc, char* argv[]) {
//     Parser pars(argc, argv);
//     Space* space = new Space();
//     srand(time(nullptr));
//     size_t numberSteps = 100000;
//     std::cout << "enter numberSteps: ";
//     std::cin >> numberSteps;
//     space->setConfig(pars.getCfgFile());
//     space->init(pars.getInpFile());
//     space->initFromParams(10000, 10000, 75);
//     // space->initFromEnergy(pars.getCfgInpFile());
//     std::string nameVtk = pars.getOutFile();
//     // Outer out("info.txt", "kinEn.dat", "potEn.dat", "fullEn.dat", "rmsVel.dat", "aVel.dat", "temp.dat");
//     space->writeVTK(nameVtk);
//     auto start = std::chrono::steady_clock::now();
//     for (size_t step = 0; step < numberSteps; ++step) {
//         std::cout << step << std::endl;
//         // if (step % 50 == 0) space->getEnergy(out, step);
//         if (space->MDStep()) {
//             space->printMol();
//             break;
//         }
//         if (step % 10 == 0) space->writeVTK(nameVtk);
//         if (step % 50 == 0) {
//             auto tmp = std::chrono::steady_clock::now();
//             std::cout << "step: " << step << "  |  " << static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(tmp - start).count()) / 1000 << " sec" << std::endl;
//         }
//     }
//     space->writeVTK(nameVtk);
//     // space->saveStruct();
//     auto tmp = std::chrono::steady_clock::now();
//     std::cout << "full duration of calculation: " << static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(tmp - start).count()) / 1000 << " sec" << std::endl;

//     delete space;
//     return 0;
// }   

