#include "Atom.h"

Atom::Atom() {}

Atom::Atom(const double* coord0, const double* vel0, const double m0, const int type_) {
    for (size_t i = 0; i < 3; ++i) {
        coord[i] = coord0[i];
        vel[i] = vel0[i];
    }
    m = m0;
    type = type_;
}

Atom::Atom(const std::vector<double>& coord0, const std::vector<double>& vel0, const double m0, const int type_) {
    for (size_t i = 0; i < 3; ++i) {
        coord[i] = coord0[i];
        vel[i] = vel0[i];
    }
    m = m0;
    type = type_;
}

int Atom::coordShift(const double dt, const double* spaceLength, bool isZ_periodic, const double hMax) {
    for (size_t i = 0; i < 3; ++i) {
        coord[i] += vel[i] * dt;
        if (atMolN2 != nullptr && i == 2 && ((coord[i] + atMolN2->coord[i]) / 2) >= hMax + 0.1) {
            // std::cout << (coord[i] + atMolN2->coord[i]) / 2 << std::endl;
            return 1;
        }
        if ((!isZ_periodic && i == 2) && (coord[i] < 0 || coord[i] > spaceLength[i])) continue;
        if (!atMolN2) {
            if (coord[i] < 0) coord[i] += spaceLength[i];
            if (coord[i] > spaceLength[i]) coord[i] -= spaceLength[i];
        }
        else {
            if (i == 2) continue;
            if (coord[i] < 0) coord[i] += spaceLength[i];
            if (coord[i] > spaceLength[i]) coord[i] -= spaceLength[i];
        }
        if ((coord[i] < 0 || coord[i] > spaceLength[i]) && !atMolN2) {
            std::cout << i << " " << coord[i] << " " << vel[i] << " " << dt << " " << type << std::endl;
            throw std::runtime_error("Problem with coordShift");
        }
    }
    return 0;
}

void Atom::velShift(const double dt) {
    for (size_t i = 0; i < 3; ++i)
        vel[i] += power[i] * dt / m;
}

double Atom::kinEnergy() {
    return m * (vel[0] * vel[0] + vel[1] * vel[1] + vel[2] * vel[2]) / 2;
}

bool Atom::checkCell(const size_t i0, const size_t j0, const size_t k0, size_t& i, size_t& j, size_t& k, const double lengthCell){
    i = static_cast<size_t>(coord[0] / lengthCell);
    j = static_cast<size_t>(coord[1] / lengthCell);
    k = static_cast<size_t>(coord[2] / lengthCell);
    if (i != i0 || j != j0 || k != k0) return true;
    return false;
}

void Atom::powerLJ(const Atom* atProb, const double* shift) {
    double r = 0;
    for (size_t i = 0; i < 3; ++i) 
        r += (coord[i] - atProb->coord[i] - shift[i]) * (coord[i] - atProb->coord[i] - shift[i]);
    r = sqrt(r);
    double force = LJ_F(r, type, atProb->type);
    double potential = LJ_P(r, type, atProb->type); 
    // General LJ ++
    ljEn += potential;
    for (size_t i = 0; i < 3; ++i) power[i] += (coord[i] - atProb->coord[i] - shift[i]) / r * force; 
} 

void Atom::powerKX(const Atom* atProb, const double* shift) {
    double r = 0;
    for (size_t i = 0; i < 3; ++i) r += (coord[i] - atProb->coord[i] - shift[i]) * (coord[i] - atProb->coord[i] - shift[i]);
    r = sqrt(r);
    double force = KX_F(r);
    double potential = KX_P(r); 
    eVib += potential;
    eRot += calcEnRot(shift);
    // kxEn += potential;

    // // General KX ++
    for (size_t i = 0; i < 3; ++i) power[i] += (coord[i] - atProb->coord[i] - shift[i]) / r * force; 
} 

double Atom::calcEnRot(const double* shift) {
    double vC[3];
    double xC[3];
    double res(0);
    for (size_t i = 0; i < 3; ++i) {
        vC[i] = (vel[i] + atMolN2->vel[i]) / 2;
        xC[i] = (coord[i] + atMolN2->coord[i] - shift[i]) / 2;
    }
    double e1[3];
    double v[3];
    for (size_t i = 0; i < 3; ++i) {
        e1[i] = coord[i] - xC[i];
        v[i] = vel[i] - vC[i];
    }
    double I = 2 * m * scalProd(e1, e1);
    std::vector<double> w(3);
    w = vecProd(e1, v);
    for (auto& x : w)
        x /= scalProd(e1, e1);

    double W2 = scalProd(w, w);
    res = I * W2 / 2;
    return res;
}

double Atom::calcEnVib() {
    double res(0);
    double r = 0;
    for (size_t ir = 0; ir < 3; ++ir) r += (coord[ir] - atMolN2->coord[ir]) * (coord[ir] - atMolN2->coord[ir]);
    r = sqrt(r);
    res = KX_P(r);
    return res;
}

double LJ_F(const double r, const int type1, const int type2) {
    return 24 * Constants::ljEps[type1][type2] * (2 * Constants::ljSigma12[type1][type2] / pow(r, 13) 
        - Constants::ljSigma6[type1][type2] / pow(r, 7));
}

double LJ_P(const double r, const int type1, const int type2) {
    return 4 * Constants::ljEps[type1][type2] * (Constants::ljSigma12[type1][type2] / pow(r, 12) 
        - Constants::ljSigma6[type1][type2] / pow(r, 6));
}

double KX_F(const double r) {
    return 2 * Constants::bondK * (Constants::bondR0 - r);
}

double KX_P(const double r) {
	return Constants::bondK * (Constants::bondR0 - r) * (Constants::bondR0 - r) / 2;
}

std::vector<double> vecProd(const std::vector<double>& A, const std::vector<double>& B) {
    std::vector<double> res(3);
    res[0] = A[1] * B[2] - A[2] * B[1];
    res[1] = A[2] * B[0] - A[0] * B[2];
    res[2] = A[0] * B[1] - A[1] * B[0];
    return res;
}

double scalProd(const std::vector<double>& A, const std::vector<double>& B) {
    double res(0);
    res += A[0] * B[0];
    res += A[1] * B[1];
    res += A[2] * B[2];
    return res;
}

std::vector<double> vecProd(double* A, double* B) {
    std::vector<double> res(3);
    res[0] = A[1] * B[2] - A[2] * B[1];
    res[1] = A[2] * B[0] - A[0] * B[2];
    res[2] = A[0] * B[1] - A[1] * B[0];
    return res;
}

double scalProd(double* A, double* B) {
    double res(0);
    res += A[0] * B[0];
    res += A[1] * B[1];
    res += A[2] * B[2];
    return res;
}